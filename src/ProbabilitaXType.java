import java.util.ArrayList;
import java.util.List;

public class ProbabilitaXType {
    private ArrayList<Probabilita> listaProbabilita;
    private ArrayList<Distanza> listaDistanze;

    public ProbabilitaXType(){
        listaProbabilita=new ArrayList<Probabilita>();
        listaDistanze=new ArrayList<Distanza>();
    }

    public ProbabilitaXType(List<Distanza> l){
        listaProbabilita=new ArrayList<Probabilita>();
        listaDistanze = new ArrayList<Distanza>(l);
    }


    public void add(Probabilita p){listaProbabilita.add(p.clone());}
    public Probabilita get(int index){return listaProbabilita.get(index);}
    public void remove(Probabilita p){listaProbabilita.remove(p);}

    public void calcolaProbabilita(){
        Probabilita p;
        int index;
        
        for(Distanza i: listaDistanze){
            p=new Probabilita(i.getFiore().getIrisClass(),(1/(double)listaDistanze.size()),listaDistanze.size());

            index = listaProbabilita.indexOf(p);
            if(index != -1){
                listaProbabilita.get(index).incrementaProbabilita();
            }else{
                listaProbabilita.add(p);
            }
        }
    }

    @Override
    public String toString(){
        String s="";

        for(Probabilita i:listaProbabilita){
            s+=i.toString()+"\n";
        }

        return s;
    }
}
