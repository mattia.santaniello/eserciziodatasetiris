public class Fiore{
    private double sepal_length;
    private double sepal_width;
    private double petal_length;
    private double petal_width;
    private String iris_class;

    public Fiore(){
        this.sepal_length=0;
        this.sepal_width=0;
        this.petal_length=0;
        this.petal_width=0;
        this.iris_class="";
    }

    public Fiore(double sepal_length,double sepal_width,double petal_length,double petal_width,String iris_class){
        this.sepal_length=sepal_length;
        this.sepal_width=sepal_width;
        this.petal_length=petal_length;
        this.petal_width=petal_width;
        this.iris_class=iris_class;
    }

    public Fiore(Fiore f){
        this.sepal_length=f.getSepalLength();
        this.sepal_width=f.getSepalWidth();
        this.petal_length=f.getPetalLength();
        this.petal_width=f.getPetalWidth();
        this.iris_class=f.getIrisClass();
    }

    public Fiore clone(){return new Fiore(this);}

    public void setSepalLength(double sepal_length){this.sepal_length=sepal_length;}
    public void setSepalWidth(double sepal_width){this.sepal_width=sepal_width;}
    public void setPetalLength(double petal_length){this.petal_length=petal_length;}
    public void setPetaWidth(double sepal_width){this.sepal_length=sepal_width;}
    public void setIrisClass(String iris_class){this.iris_class=iris_class;}

    public double distanza(double sepal_length,double sepal_width,double petal_length,double petal_width){
        return Math.sqrt(Math.pow(this.sepal_length-sepal_length,2)+Math.pow(this.sepal_width-sepal_width,2)+Math.pow(this.petal_length-petal_length,2)+Math.pow(this.petal_width-petal_width,2));
    }

    public double distanza(Fiore f){
        return Math.sqrt(Math.pow(f.getSepalLength()-this.sepal_length,2)+Math.pow(f.getSepalWidth()-this.sepal_width,2)+Math.pow(f.getPetalLength()-this.petal_length,2)+Math.pow(f.getPetalWidth()-this.petal_width,2)); 
    }

    public double getSepalLength(){return sepal_length;}
    public double getSepalWidth(){return sepal_width;}
    public double getPetalLength(){return petal_length;}
    public double getPetalWidth(){return petal_width;}
    public String getIrisClass(){return iris_class;}

    public boolean equals(Fiore f){
        return sepal_length == f.getSepalLength() && sepal_width == f.getSepalWidth() && petal_length == f.getPetalLength() && petal_width == f.getPetalWidth() && f.getIrisClass().matches(iris_class);
    }

    @Override
    public boolean equals(Object obj) {
        if(obj!=null){
            if(obj instanceof Fiore)
                return this.equals((Fiore)obj);
            else
                return false;
        }else
            return false;
    }


    @Override
    public String toString(){
        return "sepal length:"+sepal_length+"\nsepal width:"+sepal_width+"\npetal length:"+petal_length+"\npetal width:"+petal_width+"\niris class:"+iris_class;
    }
}
