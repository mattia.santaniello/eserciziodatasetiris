import java.util.Comparator;

public class Distanza implements Comparable<Distanza>{
    private Fiore fiore;
    private double distanza;

    public Distanza(Fiore f,double distanza){
        this.fiore=f.clone();
        this.distanza=distanza;
    }

    public Distanza(Distanza d){
        this.fiore=d.getFiore();
        this.distanza=d.getDistanza();
    }

    public Distanza clone(){return new Distanza(this);}

    public void setFiore(Fiore f){this.fiore=f.clone();}
    public void setDistanza(double distanza){this.distanza=distanza;}

    public Fiore getFiore(){return fiore;}
    public double getDistanza(){return distanza;}

    @Override
    public int compareTo(Distanza d2){
        return Comparator.comparing(Distanza::getDistanza).compare(this, d2);
    }

    public boolean equals(Distanza d){
        return fiore.equals(d.getFiore()) && d.getDistanza() == distanza;
    }

    @Override
    public boolean equals(Object obj){
        if(obj != null){
            if(obj instanceof Distanza)
                return equals((Distanza)obj);
            else
                return false;
        }else
            return false;
    }

    @Override
    public String toString(){
        return fiore.toString()+"\ndistanza:"+distanza+"\n";
    }
}
