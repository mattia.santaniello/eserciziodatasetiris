public class Probabilita {
    private String classe;
    private double probabilita;
    private double totale;

    public Probabilita(String classe,double probabilita,double totale){
        this.classe=classe;
        this.probabilita=probabilita;
        this.totale=totale;
    }

    public Probabilita(){
        this.classe="";
        this.probabilita=0;
        this.totale=0;
    }

    public Probabilita(Probabilita p){
        this.classe=p.getClasse();
        this.probabilita=p.getProbabilita();
        this.totale=p.getTotale();
    }

    public Probabilita clone(){return new Probabilita(this);}

    public void setClasse(String classe){this.classe=classe;}
    public void setProbabilita(double probabilita){this.probabilita=probabilita;}
    public void setTotale(double totale){this.totale=totale;}

    public String getClasse(){return classe;}
    public double getProbabilita(){return probabilita;}
    public double getTotale(){return totale;}

    public void incrementaProbabilita(){probabilita+=(1/totale);}

    
    public boolean equals(Probabilita p){
        return classe.matches(p.getClasse());
    }

    @Override
    public boolean equals(Object obj){
        if(obj != null){
            if(obj instanceof Probabilita)
                return equals((Probabilita)obj);
            else
                return false;
        }else
            return false;
    }

    @Override
    public String toString(){
        return "\nclasse di appartenenza:"+classe+"\nfrequenza:"+(int)(probabilita*totale)+
            "\nprobabilita (in percentuale) di appartenere a questa classe:"+(probabilita*100);
    }
}
