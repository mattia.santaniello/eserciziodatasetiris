import java.util.ArrayList;
import java.util.Collections;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.File;
import java.util.Scanner;

public class DataSetIris {
    private ArrayList<Fiore> dataset;

    public DataSetIris(){
        dataset=new ArrayList<Fiore>();
    }

    public DataSetIris(DataSetIris d){
        for(int i=0; i<d.size(); i++){
            dataset.add(d.get(i));
        }
    }

    public int size(){return dataset.size();}
    public void add(Fiore f){dataset.add(f.clone());}
    public void remove(Fiore f){dataset.remove(f);}
    public Fiore get(int index){return dataset.get(index).clone();}
    
    public boolean loadCSV(String filename){
        boolean risultato=false;
        File f = new File(filename);

        if(f.exists()){
            try{
                Scanner in = new Scanner(f).useDelimiter(System.lineSeparator());

                while(in.hasNext()){
                    String[] valori = in.nextLine().split(",");
                    dataset.add(new Fiore(Double.valueOf(valori[0]),Double.valueOf(valori[1]),Double.valueOf(valori[2]),Double.valueOf(valori[3]),valori[4]));
                }

                in.close();
                risultato=true;
            }catch(Exception e){
                e.printStackTrace();
            }
        }

        return risultato;
    }


    public String KNN1(double sepal_length,double sepal_width,double petal_length,double petal_width){
        return KNNK(sepal_length, sepal_width, petal_length, petal_width, 1);
    }

    public String KNNK(double sepal_length,double sepal_width,double petal_length,double petal_width,int k){
        if(k<0){
            return "";
        }else{
            ArrayList<Distanza> listaDistanze = new ArrayList<Distanza>();

            for(Fiore i: dataset){
                listaDistanze.add(new Distanza(i,i.distanza(sepal_length, sepal_width, petal_length, petal_width)));
            }

            Collections.sort(listaDistanze);
            ProbabilitaXType p;

            if(k<=listaDistanze.size())
                p = new ProbabilitaXType(listaDistanze.subList(0,k));
            else
                p = new ProbabilitaXType(listaDistanze.subList(0,listaDistanze.size()));

            p.calcolaProbabilita();
            return p.toString();
        }
    }

    @SuppressWarnings("unchecked")
    public boolean load(String filename){
        boolean risultato=false;
        File f = new File(filename);

        if(f.exists()){         
            try{
                ObjectInputStream lettore = new ObjectInputStream(new FileInputStream(f));
                dataset=(ArrayList<Fiore>)lettore.readObject();
                risultato=true;
                lettore.close();
            }catch(Exception e){
                e.printStackTrace();
            }
        }

        return risultato;
    }

    public boolean save(String filename){
        boolean risultato = false;

        try{
            ObjectOutputStream writer = new ObjectOutputStream(new FileOutputStream(new File(filename)));
            writer.writeObject(dataset);
            writer.close();
            risultato=true;
        }catch(Exception e){
            e.printStackTrace();
        }
        
        return risultato;
    }

    @Override
    public String toString(){
        String s="";

        for(Fiore i:dataset){
            s+=i.toString()+"\n\n";
        }

        return s;
    }
}