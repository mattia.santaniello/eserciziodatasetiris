public class App {
    public static void main(String[] args) throws Exception {
        DataSetIris d = new DataSetIris();
        d.loadCSV("iris.data");
        System.out.println("KNN1:\n"+d.KNN1(6.6,2.9,4.7,1.3)); 
        System.out.println("KNNK:\n"+d.KNNK(6.6,2.9,4.7,1.3,80)); 
    }
}
